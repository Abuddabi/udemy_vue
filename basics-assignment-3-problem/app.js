const app = Vue.createApp({
	data() {
		return {
			count: 0,
			result: 0
		}
	},
	computed: {

	},
	methods: {
		add5() {
			this.count = this.count + 5;
		},
		add1() {
			this.count++;
		}
	},
	watch: {
		count() {
			if (this.count < 37) {
				this.result = 'Not there yet';
			} else if (this.count > 37) {
				this.result = 'Too much!';
			} else {
				this.result = '37!';				
			}
		},
		result() {
			setTimeout(() => {
				this.result = 0;
				this.count = 0;
			}, 5000);
		}
	}
});

app.mount('#assignment');