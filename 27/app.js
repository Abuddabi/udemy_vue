const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: ''
    };
  },
  methods: {
    setName(event, lastName) {
      this.name = event.target.value + ' ' + lastName;
    },
    add(num) {
      if (num === 1) {
        this.counter++;
      } else {
        this.counter = this.counter + num;
      }
    },
    reduce(num) {
      if (num === 1) {
        this.counter--;
      } else {
        this.counter = this.counter - num;
      }
    }
  }
});

app.mount('#events');
