const app = Vue.createApp({
	data() {
		return {
			enteredTaskValue: '',
			tasks: [],
			showList: true
		};
	},
	methods: {
		addTask() {
			let value = this.enteredTaskValue.trim();
			if (!value.length) {
			  return;
			}

			this.tasks.push(value);
			this.enteredTaskValue = '';
		},
		toggleListView(event) {
			this.showList = !this.showList;
			event.target.innerText = this.showList? 'Hide List':'Show List';
		}
	}
});

app.mount('#assignment');