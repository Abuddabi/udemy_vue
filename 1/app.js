const app = Vue.createApp({
  data() {
    return {
      vueLink: 'https://vuejs.org/',
      courseGoalA: 'Finish the course and learn Vue!',
      courseGoalB: '<h2 style="color:white;">Master Vue and build amazing apps!</h2>',
    };
  },
  methods: {
    outputGoal() {
      const randomNumber = Math.random();
      if (randomNumber < 0.5) {
        return this.courseGoalA;
      } else {
        return this.courseGoalB;
      }
    }
  }
});

app.mount('#user-goal');
