function getRandomValue(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

const app = Vue.createApp({
	data() {
		return {
			// STATS
			monsterPower: [8,15],
			playerPower: [5,12],
			playerSpecialPower: [10,25],
			healPower: [8,20],

			monsterHealth: 100,
			playerHealth: 100,
			currentRound: 0,
			winner: null,
			battleLog: []
		}
	},
	computed: {
		monsterBarStyles() {
			if (this.monsterHealth < 0) {
				return {width: '0%'};
			}
			return { width: this.monsterHealth + '%' };
		},
		playerBarStyles() {
			if (this.playerHealth < 0) {
				return {width: '0%'};
			}
			return { width: this.playerHealth + '%' };
		},
		mayUseSpecialAttack() {
			return this.currentRound % 3 !== 0;
		}
	},
	watch: {
		playerHealth(value) {
			if (value <= 0 && this.monsterHealth <= 0) {
				this.winner = 'draw';
			} else if (value <= 0) {
				this.winner = 'monster';
			}
		},
		monsterHealth(value) {
			if (value <= 0 && this.playerHealth <= 0) {
				this.winner = 'draw';
			} else if (value <= 0) {
				this.winner = 'player';
			}
		}
	},
	methods: {
		startGame() {
			this.playerHealth = 100;
			this.monsterHealth = 100;
			this.currentRound = 0;
			this.winner = null;
			this.battleLog = [];
		},
		countRound() {
			this.currentRound++;
			if (this.monsterHealth > 0) {
				this.attackPlayer();
			}
		},
		attackPlayer() {
			const attackValue = getRandomValue(this.monsterPower[0],this.monsterPower[1]);
			this.playerHealth -= attackValue;
			this.addLogMessage('monster', 'attack', attackValue);
		},
		attackTemplate(min, max) {
			const attackValue = getRandomValue(min,max);
			this.monsterHealth -= attackValue;
			this.addLogMessage('player', 'attack', attackValue);
			this.countRound();
		},

		// BUTTONS
		attackMonster() {
			this.attackTemplate(this.playerPower[0],this.playerPower[1]);
		},
		specialAttackMonster() {
			this.attackTemplate(this.playerSpecialPower[0],this.playerSpecialPower[1]);
		},
		healPlayer() {
			const healValue = getRandomValue(this.healPower[0],this.healPower[1]);
			this.playerHealth += healValue;
			this.addLogMessage('player', 'heal', healValue);
			if (this.playerHealth > 100) {
				this.playerHealth = 100;
			}
			this.countRound();
		},

		surrender() {
			this.winner = 'monster';
		},
		addLogMessage(who, what, value) {
			this.battleLog.unshift({
				actionBy: who,
				actionType: what,
				actionValue: value
			});
		}
	}
});

app.mount('#game');