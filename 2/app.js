const app = Vue.createApp({
  data() {
    return {
      name: 'Egor',
      age: 28,
      randomNumber: Math.round(Math.random() * 10),
      imgSrc: 'https://w7.pngwing.com/pngs/760/624/png-transparent-google-logo-google-search-advertising-google-company-text-trademark.png'
    };
  },
  methods: {
    agePlus5() {
      return this.age + 5;
    }
  }
});

app.mount('#assignment');
