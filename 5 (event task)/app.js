const app = Vue.createApp({
	data() {
		return {
			output1: 'Output1',
			output2: 'Output2'
		}
	},
	methods: {
		showAlert() {
			alert('Alert!');
		},
		keydown(event) {
			this.output1 = event.target.value;
		},
		onEnter(event) {
			this.output2 = event.target.value;
		}
	}
});

app.mount('#assignment');