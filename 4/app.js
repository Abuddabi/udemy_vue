const app = Vue.createApp({
  data() {
    return {
      counter: 0,    
      name: ''
    };
  },
  methods: {
    resetInput() {
      this.name = '';
    },
    submitForm() {
      alert('Submitted!!');
    },
    add(num) {
      this.counter = this.counter + num;
    },
    reduce(num) {
      this.counter = this.counter - num;
    }
  }
});

app.mount('#events');
