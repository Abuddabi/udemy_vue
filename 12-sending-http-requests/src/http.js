const firebaseUrl = 'https://vue-http-demo-374ea-default-rtdb.europe-west1.firebasedatabase.app/';
const backendUrl = firebaseUrl + 'surveys.json';

function fetchFrame(method, data) {
    fetch(backendUrl, {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}

function post(data) {
    fetchFrame('POST', data);
}

function get() {
    fetch(backendUrl).then(response => {
        if (response.ok) {
            return response.json();
        }
    }).then(data => {
        return data;
    });
}

export { backendUrl, post, get };