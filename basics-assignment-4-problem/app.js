const app = Vue.createApp({
	data() {
		return {
			class1: '',
			class2: '',
			bgColor: ''
		}
	},
	computed: {
	},
	methods: {
		setClass2() {
			this.class2 = this.class2? '':'hidden';
		},
	},
	watch: {
		
	},
	created() {

	}
});

app.mount('#assignment');