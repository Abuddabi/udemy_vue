import { createRouter, createWebHistory } from "vue-router";

import TeamsList from "@/components/teams/TeamsList";
import UsersList from "@/components/users/UsersList";
import TeamMembers from "@/components/teams/TeamMembers";
import NotFound from "@/components/nav/NotFound";
import TeamsFooter from "@/components/teams/TeamsFooter";
import UsersFooter from "@/components/users/UsersFooter";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/teams' },
        {
            name: 'teams',
            path: '/teams',
            meta: { needsAuth: true },
            components: {
                default: TeamsList,
                footer: TeamsFooter
            },
            children: [
                // children got a parent path and add his own path. So here the path will be '/teams/:teamId'
                { name: 'team-members', path: ':teamId', component: TeamMembers, props: true },
            ]
        },
        {
            path: '/users',
            components: {
                default: UsersList,
                footer: UsersFooter
            },
            // beforeEnter(to, from, next) {
            //     console.log('beforeEnter in one route');
            //     console.log(to, from);
            //     next();
            // }
        },
        { path: '/:notFound(.*)', component: NotFound }
    ],
    linkActiveClass: 'active',
    scrollBehavior(_, _2,savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        return { left: 0, top: 0 };
    }
});

// router.beforeEach(function(to, _2, next) {
//     // console.log('Global beforeEach');
//     // console.log(to, from);
//     if (to.meta.needsAuth) {
//         console.log('Needs auth!');
//         next(false);
//     } else {
//         next();
//     }
//     // if (to.name === 'team-members') {
//     //     next();
//     // } else {
//     //     // here we change routing and pass configs to go to another page
//     //     next({name: 'team-members', params: {teamId: 't2'}});
//     // }
// });

// router.afterEach(function(to, from){
//     // happens after routing, can be useful for metrics and statistics for example.
//     console.log(to, from);
// });

export default router;